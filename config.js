import path from "path";

export const STATIC_PATH = path.join(__dirname, "public");
export const HTML_FILES_PATH = path.join(STATIC_PATH, "html");
export const IMG_FILES_PATH = path.join(STATIC_PATH, "assets","img");

export const PORT = 3002;
