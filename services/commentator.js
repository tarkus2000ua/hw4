import {
  getLeaders,
  getFinishedRacers,
  getLastLapRacers
} from '../services/roomService.js';
const CHECK_LEADERS_INTERVAL = 30;
import {
  SECONDS_FOR_GAME
} from '../socket/config.js';

export class Commentator {
  constructor(room, io) {
    this.timer = null;
    this.room = room;
    this.io = io;
    this.finishedRacers = 0;
    this.lastLapRacers = [];
  }

  start() {
    let time = 0;
    this.sendMessage(this.onStart());
    this.timer = setInterval(() => {
      if (time % CHECK_LEADERS_INTERVAL === 0 && time > 0 && time !== SECONDS_FOR_GAME) {
        this.sendMessage(this.onTimer());
      }

      let finished = this.onFinish()
      if (finished) {
        this.sendMessage(finished);
      }

      let lastLap = this.onLastLap()
      // console.log('laslap');
      // console.log(lastLap);
      if (lastLap) {
        this.sendMessage(lastLap);
      }

      if (time > SECONDS_FOR_GAME) {
        this.clearTimer(timer);
      }
      time++;
    }, 1000)
  }

  onStart() {
    const racers = getLeaders(this.room).map(racer => racer.name);
    let racersList = racers.slice(0, -1).join('</b>, <b>') + ' та ' + racers.splice(-1, 1).join('</b>, <b>');
    return `На вулиці зараз хмарно, але на нашому майданчику чудова атмосфера: двигуни ревуть, вболівальники теж ревуть, а учасники гонки спокійно готуються до старту. Коментувати змагання сьогодні буду я, Василіна Ангуляр. Вітаю вас панове! Сьогоднішні учасники гонки: <b>${racersList}</b>`
  }

  onTimer() {
    const leaders = getLeaders(this.room);
    return `<b>${leaders[0].name}</b> на білому коні зараз попереду${leaders[1]?`, за ним йде <b>${leaders[1].name}</b>`:``}${leaders[2]?`, а третім йде <b>${leaders[2].name}</b>`:``}`;
  }

  onEndGame() {
    let silver = '';
    let bronze = '';

    const leaders = getLeaders(this.room);

    let gold = `Перше місце посідає <b>${leaders[0].name}</b>`;
    if (leaders[0].time) {
      gold += ` з часом ${leaders[0].time} секунд`;
    } else {
      gold += `, фінішувати він не встиг.`
    }

    if (leaders[1]) {
      silver = ` Cеребро у <b>${leaders[1].name}</b>`;
      if (leaders[1].time) {
        silver += ` , його результат ${leaders[1].time} секунд.`;
      } else {
        silver += `, який не подолав всю дистанцію.`
      }
    }

    if (leaders[2]) {
      bronze = ` А бронзовим призером стає <b>${leaders[2].name}</b>`;
      if (leaders[2].time) {
        bronze += ` з часом ${leaders[2].time} секунд.`;
      } else {
        bronze += `,  фінішу він не досягнув.`
      }
    }

    let finanlSpeech = gold + silver + bronze;
    if (leaders[3]) {
      finanlSpeech += ` Іншим, на жаль, сьогодні не пощастило. Успіхів в наступних змаганнях!`
    }
    return finanlSpeech;
  }

  onLastLap() {
    const racersToAnnounce = getLastLapRacers(this.room);
    console.log(racersToAnnounce);
    let result = null;
    let sendMessage = false;
    
    if (racersToAnnounce?.length > 0) {
      
      racersToAnnounce.forEach(racer => {
        if (this.lastLapRacers.length === 0) {
          sendMessage = true;
        } else {
          if (this.lastLapRacers.findIndex(r => r.name === racer.name) === -1) {
            sendMessage = true;
          }
        }

        if(sendMessage){
          this.lastLapRacers.push(racer);
          const leaders = getLeaders(this.room);
          result = `Фініш вже близько і здається, що першим його перетне <b>${leaders[0].name}</b>. ${leaders[1]?`  За друге місце змагаються <b>${leaders[1].name}</b>`:``} ${leaders[2]?`, а третім йде <b>${leaders[2].name}</b>`:`і позаду більше нікого`}. Але дочекаймося фінішу. `;
        }
      })

    }
    return result;
  }

  onFinish() {
    const finished = getFinishedRacers(this.room);
    if (finished?.length > this.finishedRacers) {
      this.finishedRacers++;
      return `Фінішну лінію перетинає <b>${finished[0].name}</b>`;
    }
  }

  sendMessage(message) {
    this.io.to(this.room).emit('commentator', message);
  }

  clearTimer() {
    this.sendMessage(this.onEndGame());
    clearInterval(this.timer);
  }
}