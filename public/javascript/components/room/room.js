import { exitRoom,updateUserStatus } from '../../socket/socketHandler.js';
import { createElement } from '../../helpers/domHelper.js';
import { Player } from '../../game.mjs';
import { createCommentator } from '../commentator/commentator.js';

const container = document.getElementById('rooms-page');

export const renderRoom = (room) => {
  container.innerHTML='';
  const roomDiv = createRoom(room);
  container.append(roomDiv);
}

const createRoom = (room) => {
  const roomWrapper = createElement({ tagName: 'div', className: 'room-wrapper' });
  const leftPanel = createLeftPanel(room);
  const rightPanel = createRightPanel(room);
  const commentator = createCommentator();
  roomWrapper.append(leftPanel,rightPanel,commentator);
  return roomWrapper;
}

export const createUserList = (users) => {
  
  const userList = createElement({ tagName: 'div', className: 'user-list' });
  users.forEach(user => {
    const userDiv = createUser(user);
    userList.appendChild(userDiv);
  });
  return userList;
}

const createUser = (user) => {
    const userDiv = createElement({ tagName: 'div', className: 'user' });
    const userStatus = createElement({ tagName: 'div', className: 'user-status' });
    const userReady = createElement({ tagName: 'div', className: `user-ready ${user.isready ? 'ready':''}` });
    const userName = createElement({ tagName: 'div', className: 'user-name' });
    const progressBar = createElement({ tagName: 'progress', className: 'progress-bar',attributes: { value: user.progress, max: '100' } });
    
    userName.innerText = user.name;
    if (user.name === Player.name){
      userName.innerText+=' (You)';
    }

    userStatus.append(userReady,userName);
    userDiv.append(userStatus,progressBar);
    return userDiv;
}

const createLeftPanel = (room) => {
  const leftPanel = createElement({ tagName: 'div', className: 'left-panel' });
  const pageHeader = createElement({ tagName: 'h1', className: 'page-header' });
  const backBtn = createElement({ tagName: 'button', className: 'btn back' });
  
  backBtn.innerText='Back to Rooms';
  
  const userList = createUserList(room.users);
  leftPanel.append(pageHeader,backBtn,userList);
  
  leftPanel.querySelector('.btn.back').addEventListener('click',async ()=>{
    exitRoom(room.name);
  });
  return leftPanel;
}

const createRightPanel = (room) => {
    const rightPanel = createElement({ tagName: 'div', className: 'right-panel' });
    const readyBtn = createElement({ tagName: 'button', className: 'btn ready',attributes: { value: Player.isready} });
    const timer = createElement({ tagName: 'div', className: 'timer hide', attributes: { id: "timer"} });
    const textBox = createElement({ tagName: 'div', className: 'text-box hide', attributes: { id: "text-box"} });
    const timeLeft = createElement({ tagName: 'div', className: 'time-left hide', attributes: { id: "time-left"} });
    
    readyBtn.innerText=Player.isready?'Not Ready':'Ready';
    rightPanel.append(readyBtn,timer,textBox,timeLeft);

    rightPanel.querySelector('.btn.ready').addEventListener('click',async (event)=>{
      updateUserStatus(room.name, event.target.value);
    });
    return rightPanel;
}