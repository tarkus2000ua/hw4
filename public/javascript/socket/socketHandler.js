import { socket } from '../game.mjs';

export const joinRoom = (room) => {
  socket.emit('joinRoom', room );
}

export const createRoom = (room) => {
  socket.emit('createRoom', room );
}

export const exitRoom = (room) => {
  socket.emit('exitRoom',  room );
}

export const updateUserStatus = (room, status) => {
  socket.emit('userStatus', {room,status} );
}

export const updateUserProgress = (room, progress, charsLeft) => {
  socket.emit('userProgress', {room,progress,charsLeft} );
}

export const updateUserTime = (room, time) => {
  socket.emit('userTime', {room,time} );
}