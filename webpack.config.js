const path = require('path');
const nodeExternals = require('webpack-node-externals');

const PATH_SRC = path.join(__dirname, './');
const PATH_DIST = path.join(__dirname, './');

module.exports = {
  target: 'node', // ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()], // ignore all modules in node_modules folder
  entry: [`${PATH_SRC}server.js`],
  output: {
    path: PATH_DIST,
    publicPath: './',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};